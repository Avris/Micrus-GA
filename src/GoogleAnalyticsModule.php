<?php
namespace Avris\Micrus\GoogleAnalytics;

use Avris\Micrus\Bootstrap\ModuleInterface;
use Avris\Micrus\Bootstrap\ModuleTrait;
use Avris\Micrus\Tool\Config\ParametersProvider;

final class GoogleAnalyticsModule implements ModuleInterface, ParametersProvider
{
    use ModuleTrait;

    public function getParametersDefaults(): array
    {
        return [
            'GA_TRACKING_ID' => '',
        ];
    }
}
