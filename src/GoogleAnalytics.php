<?php
namespace Avris\Micrus\GoogleAnalytics;

use Avris\Micrus\Bootstrap\Terminator;
use Avris\Http\Request\RequestInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

final class GoogleAnalytics extends AbstractExtension
{
    const CODE = <<<HTML
<script async src="https://www.googletagmanager.com/gtag/js?id=%s"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', '%s');
</script>
HTML;

    const COLLECT_URL = 'https://www.google-analytics.com/collect';

    /** @var string */
    private $trackingId;

    /** @var array[] */
    private $queue = [];

    public function __construct(string $envGaTrackingId, Terminator $terminator)
    {
        $this->trackingId = $envGaTrackingId;
        $terminator->attach(function () {
            foreach ($this->queue as $options) {
                $this->sendReport($options);
            }
        });
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('googleAnaytics', function () {
                return $this->trackingId ? sprintf(static::CODE, $this->trackingId, $this->trackingId) : '';
            },  ['is_safe' => ['html']]),
        ];
    }

    public function collect(RequestInterface $request, bool $deferred = true): bool
    {
        if (!$this->trackingId) {
            return false;
        }

        $data = [
            'v' => 1,
            'tid' => $this->trackingId,
            'cid' => sha1(serialize($request)),
            't' => 'pageview',
            'dl' => $request->getFullUrl(),
            'ul' => ($languages = $request->getHeaders()->getLanguage()) ? $languages->getBest() : null,
            'de' => ($charset = $request->getHeaders()->getCharset()) ? $charset->getBest() : null,
        ];

        $options = [
            CURLOPT_URL => static::COLLECT_URL,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_POST => true,
            CURLOPT_HTTPHEADER => ['Content-type: application/x-www-form-urlencoded'],
            CURLOPT_USERAGENT => (string) $request->getHeaders()->getUserAgent(),
            CURLOPT_POSTFIELDS => utf8_encode(http_build_query($data)),
            CURLOPT_RETURNTRANSFER => true,
        ];

        if ($deferred) {
            $this->queue[] = $options;

            return true;
        }

        $this->sendReport($options);

        return true;
    }

    private function sendReport($options)
    {
        $ch = curl_init();
        curl_setopt_array($ch, $options);
        curl_exec($ch);
        curl_close($ch);
    }
}
