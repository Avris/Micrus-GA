# Micrus Google Analytics Service ##

This is a module for [Micrus framework](https://micrus.avris.it)
that adds a really basic helper for Google Analytics.

## Installation

Run:

    composer require avris/micrus-ga

Then register the module in your `App\App:registerModules`: 

    yield new \Avris\Micrus\GoogleAnalytics\DoctrineModule;

    
You will be asked for a GA Tracking-ID and it will be saved in the `.env` file.
If you don't provide any (for instance in a testing environment),
this service will not fail, it will just do nothing.

## Usage ###

To include a tracking code in your template, just write before the closing `</body>`:

    {{ googleAnaytics() }}

To report a visit from the server side, you can run this code:

    public function generateImageAction(RequestInterface $request, GoogleAnalytics $ga)
    {
        // ...
        $ga->collect($request);
        // ...
    }

For performance, that data will be sent during request termination
(so in the background, after the response has already been sent).
You can force the service to send the data right away with `$ga->collect($request, false)`.

## Copyright

* **Author:** Andre Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
